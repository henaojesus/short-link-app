import { Mongo } from 'meteor/mongo';

export const Links = new Mongo.Collection('links');

Links.allow({
  insert() { return true; },
  update() { return true; },
  remove() { return true; }
});