import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import SimpleSchema from 'simpl-schema';

Accounts.validateNewUser((user) => {
  const email = user.emails[0].address;

  try {
    new SimpleSchema({
      email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email
      }
    }).validate({ email });
  }
  catch (error) {
    throw new Meteor.Error(400, error.message);
  }
  return true;
});


/* const petSchema = new SimpleSchema({
    name: {
      type: String,
      min: 1,
      max: 100,
      optional: true
    },
    age: {
      type: Number,
      min: 0
    },
    contactNumber: {
      type: String,
      optional: true,
      regEx: SimpleSchema.RegEx.Phone
    }
  });

  const employeeSchema = new SimpleSchema({
    name: {
      type: String,
      min: 1,
      max: 200
    },
    hourlyWage: {
      type: Number,
      min: 1
    },
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.Email
    }
  });
  petSchema.validate({
    age: 21,
    contactNumber: '1234'
  }); */